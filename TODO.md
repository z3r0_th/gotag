#Doing

- Make collision detection smarter
  - An entity that knows where players are and if they are going to cross path, and if so if there should be a collision
  
- PlayerMovement should trigger events as act on movement (starting movement, finishing movement, direction, etc)
- Blink edge lights

#Next

- Test on phone (specially movement commands)
- player movement effects and particles
- player movement sounds
- basic game mechanics
  - steal crown
  - when stealing, make it freeze for a moment
  - scoring (who has the crown makes more points)
- game controller (time, starting, who win/lose)
- UI 
- main menu

#Backlog

- match make
- special obstacles in the environment

#Bug

# Environment 

## Circus

- Improve Flag positioning and coloring
-- At the moment the Flags might be positioning facing the same direction or having the same color
--- We should improve this by looking into the neighbours position and/or colors. Also, the offset should be more randomized (they should NOT be in the same level)

## Art

- button_focus -> This one is in a wrong size relative to the others, we should fix it to have a good Highlight/Selected sprite state

# Done

- Make collision detection smarter
  [x] An initial implementation should be a simple collision detection
[x] Player's should not consider each other as an obstacle (make a detection layer filter)
[x] player selection (change player sprite) - logic part is working