# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added 

Player Character Selection 

- Added the Main Menu - with "Run" button leading to next scene (Character Selection)
- Added the Character Selection menu - with "back" leading back to "Main Menu" and "Play" leading to "Game" scene
- Added a way to load scene 
-- GameScene to reference to a scene, instead of pointing to scene through a string (This needs improvement to make the asset update when the scene asset is updated)
- Added Character Toggle Button - To select character based on CharacterProperties 
- Added Character Selection Group - to update the Player character property with the one selected

Player can have a Character object reference to modify the character information (sprite and name)

- Add new assets (Bird and Cat)
- Add Scriptable for Character (name, body, head and smile head sprites) - all 6 characters
- some refactor, moving character sprites from "UI" to "UI/Character"


## [0.1.0] - 2023-07-03

### Added

Collision might not work properly due to skip some Update methods - this has to be improved

#### Files and responsabilities 

- GameEventTemplate - to be able to make gameEvent with parameters
- GameEventGo - a game event with a GameObject as parameter
- PlayerCollisionDetectionManager - responsible to detect collision between players
- PlayerGameEvent - game event with a Player as parameter
- Game - An overall game mechanics manager, adds a collision manager

### Changed

- Spawner - triggers a player event

--- 

## [TAG] - YYYY-MM-DD

### Added
- for new features. 
### Changed
- for changes in existing functionality. 
### Deprecated
- for soon-to-be removed features. 
### Removed
- for now removed features. 
### Fixed
- for any bug fixes. 
### Security
- in case of vulnerabilities. 

#### Guiding Principles

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

#### Keep an Unreleased section at the top to track upcoming changes.
- This serves two purposes:
-- People can see what changes they might expect in upcoming releases
-- At release time, you can move the Unreleased section changes into a new release version section.