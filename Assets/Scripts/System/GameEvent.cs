using UnityEngine;

namespace System
{
    [CreateAssetMenu(fileName = "Game Event", menuName = "Game/GameEvent/Event", order = 0)]
    public class GameEvent : ScriptableObject
    {
        private event Action Event;
        
        public event Action EventAction
        {
            add => Event += value;
            remove => Event -= value;
        }

        public void Invoke()
        {
            Debug.Log($"<color=blue>>> Event \"<i>{name}</i>\" Raised</color>");
            Event?.Invoke();
        }

        public void Clear()
        {
            Event = null;
        }
    }
}
