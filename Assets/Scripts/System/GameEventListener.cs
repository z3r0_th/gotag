using UnityEngine;
using UnityEngine.Events;

namespace System
{
    public class GameEventListener : MonoBehaviour
    {
        [SerializeField] private GameEvent gameEvent;
        [SerializeField] private UnityEvent eventListener;

        private void OnEnable()
        {
            gameEvent.EventAction += OnEventRaised; 
        }
        
        private void OnDisable()
        {
            gameEvent.EventAction -= OnEventRaised; 
        }

        private void OnEventRaised()
        {
            eventListener.Invoke();
        }

        private void Start()
        {
            gameEvent.Invoke();
        }
    }
}
