﻿using UnityEngine;

namespace System
{
    public class GameEventTemplate<T> : ScriptableObject
    {
        private event Action<T> Event;
        
        public event Action<T> EventAction
        {
            add => Event += value;
            remove => Event -= value;
        }

        public void Invoke(T param)
        {
            Debug.Log($"<color=blue>>> Event \"<i>{name}</i>\" Raised</color>");
            Event?.Invoke(param);
        }

        public virtual void Invoke()
        {
            
        }

        public void Clear()
        {
            Event = null;
        }
    }
}