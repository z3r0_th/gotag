﻿using UnityEngine;

namespace System
{
    [CreateAssetMenu(fileName = "Game Event GameObject", menuName = "Game/GameEvent/GameObject", order = 0)]
    public class GameEventGo : GameEventTemplate<GameObject>
    {
        [SerializeField] private GameObject go;
        public override void Invoke()
        {
            Invoke(go);
        }
    }
}