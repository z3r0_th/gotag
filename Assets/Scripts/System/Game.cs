﻿using System.Collections.Generic;
using UnityEngine;
using Player;
using Player.Event;

namespace System
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private PlayerCollisionDetectionManager collisionDetectionManager;
        [SerializeField] private List<Player.Player> players = new();
        [SerializeField] private PlayerGameEvent[] playerSpawners;
        [SerializeField] private GameObject crownPrefab;

        private Player.Player _currentCrownedPlayer;
        private GameObject _crown;

        private void Reset()
        {
            collisionDetectionManager = GetComponent<PlayerCollisionDetectionManager>();
            if (collisionDetectionManager == null)
            {
                collisionDetectionManager = gameObject.AddComponent<PlayerCollisionDetectionManager>();
            }
        }

        private void Start()
        {
            _crown = Instantiate(crownPrefab);
            _crown.gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            players.Clear();
            foreach (var playerSpawner in playerSpawners)
            {
                playerSpawner.EventAction += PlayerSpawned;
            }
            collisionDetectionManager.CollisionEvent += OnCollisionEvent;
        }

        private void OnCollisionEvent(Player.Player p1, Player.Player p2)
        {
            if (p1.HasCrown)
            {
                SetCrownToPlayer(p2);
            } else if (p2.HasCrown)
            {
                SetCrownToPlayer(p1);
            }
        }

        private void OnDisable()
        {
            foreach (var playerSpawner in playerSpawners)
            {
                playerSpawner.EventAction -= PlayerSpawned;
            }
            collisionDetectionManager.CollisionEvent -= OnCollisionEvent;
        }

        private void PlayerSpawned(Player.Player player)
        {
            players.Add(player);
            collisionDetectionManager.SetPlayers(players.ToArray());
            if (players.Count == playerSpawners.Length)
            {
                StartGame();
            }
        }

        private void StartGame()
        {
            var randomPlayerCrown = UnityEngine.Random.Range(0, players.Count);
            var player = players[randomPlayerCrown]; 
            if (player == null)
            {
                Debug.LogError("Player is null!");
                return;
            }

            SetCrownToPlayer(player);
        }

        private void SetCrownToPlayer(Player.Player player)
        {
            var playerTransform = player.transform;
            _crown.transform.position = playerTransform.position;
            _crown.transform.SetParent(playerTransform, true);
            _crown.gameObject.SetActive(true);
            if (_currentCrownedPlayer != null)
            {
                _currentCrownedPlayer.HasCrown = false;
            }
            
            player.HasCrown = true;
            _currentCrownedPlayer = player;
        }
    }
}