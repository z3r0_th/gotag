using Player;
using Player.Event;
using UnityEngine;

namespace System
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private PlayerGameEvent spawnEvent;
        [SerializeField] private Player.PlayerProperties playerProperties;
        [SerializeField] private Player.Player playerPrefab;
    
        void Start()
        {
            var player = Instantiate(playerPrefab);
            player.transform.position = transform.position;
            player.PlayerProperties = playerProperties;
            spawnEvent.Invoke(player);
        }
    }
}
