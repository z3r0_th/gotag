using Unity.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace System
{
    [CreateAssetMenu]
    public class GameScene : ScriptableObject
    {
        [SerializeField] private string sceneName;
        
#if UNITY_EDITOR
        [SerializeField] private SceneAsset scene;
        private void OnValidate()
        {
            if (scene == null)
            {
                return;
            }
            sceneName = scene.name;
        }
#endif

        public string SceneName => sceneName;

        public static implicit operator string(GameScene scene)
        {
            return scene.sceneName;
        }
    }
}
