using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class LoadScene : MonoBehaviour
    {
        [SerializeField] private GameScene sceneAsset;

        public void LoadNextScene()
        {
            SceneManager.LoadScene(sceneAsset, LoadSceneMode.Single);
        }
    }
}
