using Player;
using UnityEngine;

namespace UI
{
    public class CharacterSelectionGroup : MonoBehaviour
    {
        [SerializeField] private PlayerProperties playerProperties;
        [SerializeField] private RectTransform characterOptions;

        private void Start()
        {
            foreach (RectTransform characterOption in characterOptions)
            {
                var toggle = characterOption.GetComponent<CharacterSelectionButton>();
                toggle.OnActive += OnCharacterSelected;
            }
        }

        private void OnCharacterSelected(CharacterProperties characterProperties)
        {
            playerProperties.Character = characterProperties;
        }
    }
}
 