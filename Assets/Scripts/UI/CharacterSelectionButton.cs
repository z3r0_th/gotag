using System;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CharacterSelectionButton : MonoBehaviour
    {
        public Action<CharacterProperties> OnActive;
        
        [SerializeField] private CharacterProperties characterProperties;
        [SerializeField] private Image characterHead;
        [SerializeField] private Toggle toggleButton;
        
        void Start()
        {
            if (toggleButton == null)
            {
                return;
            }
            toggleButton.onValueChanged.AddListener(CharacterButtonToggled);
            SetCharacterHead(toggleButton.isOn);
        }

        private void CharacterButtonToggled(bool active)
        {
            SetCharacterHead(active);
        }

        private void SetCharacterHead(bool smiling)
        {
            characterHead.sprite = smiling ? characterProperties.CharacterHeadSmileSprite : characterProperties.CharacterHeadSprite;
            if (smiling && Application.isPlaying)
            {
                OnActive?.Invoke(characterProperties);
            }
        }

        private void OnValidate()
        {
            if (toggleButton == null || characterHead == null || characterProperties == null)
            {
                return;
            }
            SetCharacterHead(toggleButton.isOn);
        }
    }
}
