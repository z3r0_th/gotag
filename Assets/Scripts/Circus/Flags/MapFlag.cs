using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace Circus.Flags
{
    public class MapFlag : MonoBehaviour
    {
        [SerializeField] private Tilemap map;
        [SerializeField] private TileFlagDescription[] tileFlags;
        [SerializeField] private Color[] flagColors;
        [Range(0, 100)][SerializeField] private float emptyFlagProbability;

        private GameObject _flags;
        private Dictionary<TileBase, TileFlagDescription> _flagMap;

        private void Start()
        {
            _flagMap = new Dictionary<TileBase, TileFlagDescription>();
            foreach (var tileFlag in tileFlags)
            {
                _flagMap.Add(tileFlag.Tile, tileFlag);
            }

            _flags = new GameObject("Flags");
            PopulateFlags();
        }

        private Color GetRandomFlagColor()
        {
            return flagColors[Random.Range(0, flagColors.Length)];
        }

        private void PopulateFlags()
        {
            for (var n = map.cellBounds.xMin; n < map.cellBounds.xMax; n++)
            {
                for (var p = map.cellBounds.yMin; p < map.cellBounds.yMax; p++)
                {
                    var showProbability = Random.Range(0, 100);
                    if (showProbability < emptyFlagProbability)
                    {
                        continue;
                    }
                    var localPlace = (new Vector3Int(n, p, (int)map.transform.position.y));
                    if (!map.HasTile(localPlace))
                    {
                        continue;
                    }

                    var tileBase = map.GetTile(localPlace);
                    if (!_flagMap.ContainsKey(tileBase))
                    {
                        continue;
                    }
                    
                    var tileFlag = _flagMap[tileBase];
                    var tilePosition = map.CellToWorld(localPlace);
                    var flag = tileFlag.InstantiateFlag();
                    var flagPosition = tileFlag.GetFlagPosition();
                    flag.transform.position = tilePosition + flagPosition.offset;
                    flag.transform.rotation = Quaternion.Euler(0,0,flagPosition.rotation);
                    var flagSpriteRenderer = flag.GetComponent<SpriteRenderer>();
                    if (flagSpriteRenderer != null)
                    {
                        flagSpriteRenderer.color = GetRandomFlagColor();
                    }
                    flag.transform.SetParent(_flags.transform);
                }
            }
        }
    }
}
