using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace Circus.Flags
{
    [Serializable]
    public struct TileFlagPosition
    {
        public float rotation;
        public Vector3 offset;
    }
    
    [Serializable]
    public struct PossibleFlags
    {
        [SerializeField] private GameObject[] flags;

        public GameObject GetFlag()
        {
            return flags[Random.Range(0, flags.Length)];
        }
    }
    
    [CreateAssetMenu(fileName = "flag", menuName = "Level/Circus/Flag")]
    public class TileFlagDescription : ScriptableObject
    {
        [SerializeField] private TileBase tile;
        [SerializeField] private PossibleFlags flags;
        [SerializeField] private TileFlagPosition[] flagPositions;

        public TileBase Tile => tile;

        public TileFlagPosition GetFlagPosition()
        {
            return flagPositions[Random.Range(0, flagPositions.Length)];
        }

        public GameObject InstantiateFlag()
        {
            return Instantiate(flags.GetFlag());
        }
    }
}
