﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Circus.Lights
{
    public class LightsManager : MonoBehaviour
    {
        [SerializeField] private MapLight mapLight;
        [SerializeField]private float defaultLightFrequency = 0.25f;
        
        private List<TileLight> _lights;
        

        private void Start()
        {
            _lights = mapLight.lights;
            AlternateLights();
        }

        public void AllLightsOn()
        {
            StopAllCoroutines();
            foreach (var tileLight in _lights)
            {
                tileLight.SetLight(true);
            }
        }

        public void AllLightsOff()
        {
            StopAllCoroutines();
            foreach (var tileLight in _lights)
            {
                tileLight.SetLight(false);
            }
        }

        /// <summary>
        /// Light will turn on in a sequence (one on, one off)
        /// Lights will alternate on it's state (on off) in a given frequency for a given time
        /// When time is finished, all lights will be turned off
        /// </summary>
        /// <param name="frequency">how fast lights should change its state</param>
        /// /// <param name="time">how long should this animation last, negative number is infinite</param>
        public void AlternateLights(float frequency = -1, float time = -1)
        {
            if (frequency <= 0)
            {
                frequency = defaultLightFrequency;
            }

            StopAllCoroutines();
            StartCoroutine(_AlternateLights(frequency, time));
        }

        private void TurnEvenLights(bool onOff)
        {
            for (var i = 0; i < _lights.Count; ++i)
            {
                var tileLight = _lights[i];
                if (tileLight == null)
                {
                    continue;
                }

                var isEven = i % 2 == 0;
                var isOn = onOff ? isEven : !isEven;
                tileLight.SetLight(isOn);
            }
        }

        private IEnumerator _AlternateLights(float frequency, float time)
        {
            var animationTime = time < 0 ? 1.0f : time;
            var animationFrequency = frequency;
            var even = true;
            TurnEvenLights(even);
            while (animationTime > 0)
            {
                if (animationFrequency <= 0)
                {
                    animationFrequency = frequency;
                    even = !even;
                    TurnEvenLights(even);
                } 
                
                animationFrequency -= Time.deltaTime;
                animationTime -= Time.deltaTime;
                if (time < 0)
                {
                    animationTime = 1;
                }
                yield return null;
            }
        }
    }
}