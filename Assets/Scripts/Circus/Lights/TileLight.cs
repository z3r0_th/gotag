﻿using System;
using UnityEngine;

namespace Circus.Lights
{
    public class TileLight : MonoBehaviour
    {
        [SerializeField] private Transform lightTransform;
        private bool _isLightOn = false;

        public Transform LightTransform
        {
            get => lightTransform;
            set
            {
                lightTransform = value;
                IsLightOn = false;
            }
        }

        public bool IsLightOn
        {
            get => _isLightOn;
            set
            {
                SetLight(value);
            }
        }
        
        private void Awake()
        {
            LightTransform = transform;
        }

        public void LightOn()
        {
            if (LightTransform == null)
            {
                return;
            }
            LightTransform.gameObject.SetActive(true);
        }
        
        public void LightOff()
        {
            if (LightTransform == null)
            {
                return;
            }
            LightTransform.gameObject.SetActive(false);
        }

        public void SetLight(bool onOff)
        {
            _isLightOn = onOff;
            if (onOff)
            {
                LightOn();
            }
            else
            {
                LightOff();
            }
        }
    }
}