using UnityEngine.Tilemaps;
using UnityEngine;
using System;

namespace Circus.Lights
{
    [Serializable]
    public struct TileLightPosition
    {
        public float rotation;
        public Vector3 offset;
    }
    
    [CreateAssetMenu(fileName = "flag", menuName = "Level/Circus/Light")]
    public class TileLightDescription : ScriptableObject
    {
        [SerializeField] private TileLightPosition lightPosition;
        [SerializeField] private GameObject light;
        [SerializeField] private TileBase tile;
        
        public TileLightPosition LightPosition => lightPosition;
        public TileBase Tile => tile;

        public GameObject InstantiateLight()
        {
            var lightInstance = Instantiate(light);
            lightInstance.transform.rotation = Quaternion.Euler(0,0,lightPosition.rotation);
            return lightInstance;
        }
    }
}
