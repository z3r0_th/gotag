using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine.Tilemaps;
using UnityEngine;

namespace Circus.Lights
{
    public class MapLight : MonoBehaviour
    {
        [Serializable]
        private struct LightList
        {
            public TileBase tile;
            public bool reverse;
        }
        
        [SerializeField] private Tilemap map;
        [SerializeField] private TileLightDescription[] tileLights;
        [SerializeField] private LightList[] supportedLightTiles;

        private GameObject _lights;
        private Dictionary<TileBase, TileLightDescription> _lightMap;
        public List<TileLight> lights;

        private void Awake()
        {
            _lightMap = new Dictionary<TileBase, TileLightDescription>();
            foreach (var tileLight in tileLights)
            {
                _lightMap.Add(tileLight.Tile, tileLight);
            }

            _lights = new GameObject("Lights");
            lights = new List<TileLight>();
            PopulateFlags();
        }

        private void PopulateFlags()
        {
            Dictionary<TileBase, List<GameObject>> tiles = new();
            foreach (var supportedLightTile in supportedLightTiles)
            {
                tiles.Add(supportedLightTile.tile, new List<GameObject>());
            }

            for (var n = map.cellBounds.xMin; n < map.cellBounds.xMax; n++)
            {
                for (var p = map.cellBounds.yMin; p < map.cellBounds.yMax; p++)
                {
                    var localPlace = (new Vector3Int(n, p, (int)map.transform.position.y));
                    if (!map.HasTile(localPlace))
                    {
                        continue;
                    }

                    var tileBase = map.GetTile(localPlace);
                    if (!_lightMap.ContainsKey(tileBase))
                    {
                        continue;
                    }

                    var tileLight = _lightMap[tileBase];
                    var tilePosition = map.CellToWorld(localPlace);
                    var lightGo = tileLight.InstantiateLight();
                    if (!tiles.TryGetValue(tileBase, out var list))
                    {
                        continue;
                    }
                    list.Add(lightGo);
                    lightGo.transform.position = tilePosition + tileLight.LightPosition.offset;
                }
            }
            foreach(var entry in supportedLightTiles)
            {
                var list = tiles[entry.tile];
                if (entry.reverse)
                {
                    list.Reverse();
                }
                foreach (var go in list)
                {
                    go.transform.SetParent(_lights.transform);
                }
            }
            
            foreach(Transform lightGo in _lights.transform)
            {
                lights.Add(lightGo.gameObject.AddComponent<TileLight>());
            }
        }
    }
}
