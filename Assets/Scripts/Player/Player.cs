using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement), typeof(PlayerControl))]
    public class Player : MonoBehaviour
    {
        public PlayerProperties PlayerProperties
        {
            get => _properties;
            set
            {
                _properties = value;
                _playerControl.MovementInputReference = _properties.PlayerMovementInputReference;
                if (_spriteRenderer != null)
                {
                    _spriteRenderer.sprite = _properties.Character.CharacterSprite;
                }
            }
        }
        public bool HasCrown { get; set; }
        private PlayerProperties _properties;
        
        public PlayerMovement PlayerMovement => _playerMovement;
        private PlayerMovement _playerMovement;

        private PlayerControl _playerControl;
        private SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _playerControl = GetComponent<PlayerControl>();
            _playerMovement = GetComponent<PlayerMovement>();
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }

        public Bounds Bounding => _spriteRenderer.bounds;
    }
}

