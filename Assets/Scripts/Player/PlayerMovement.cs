using System;
using UnityEngine;

namespace Player
{
    public enum MoveDirection
    {
        None,
        Up,
        Down,
        Right,
        Left
    }
    
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float speed = 1f;
        [SerializeField] private LayerMask mask;
        
        private Vector2 _lastHitPoint;
        private Vector2 _destination;
        private bool _moving;
        
        public void Move(MoveDirection direction)
        {
            if (_moving)
            {
                return;
            }
            switch (direction)
            {
                case MoveDirection.Up: MoveUp();
                    break;
                case MoveDirection.Down: MoveDown();
                    break;
                case MoveDirection.Right: MoveRight();
                    break;
                case MoveDirection.Left: MoveLeft();
                    break;
                case MoveDirection.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
        
        private void Start()
        {
            _moving = false;
        }
        
        private void Update()
        {
            AnimateMovement();
        }

        private void MoveUp()
        {
            StartMovement(Vector2.up);
        }

        private void MoveDown()
        {
            StartMovement(Vector2.down);
        }
        
        private void MoveRight()
        {
            StartMovement(Vector2.right);
        }

        private void MoveLeft()
        {
            StartMovement(Vector2.left);
        }
        
        private void StartMovement(Vector2 direction)
        {
            var origin = transform.position; 
            Debug.DrawRay(origin, direction*1000, Color.green, 2f);
            var hit = Physics2D.Raycast(origin, direction, Mathf.Infinity, mask.value);
            if (hit.collider == null)
            {
                Debug.LogError(">> Should always collide something");
                return;
            }
        
            if (Vector2.Distance(_lastHitPoint, hit.point) < 0.1f)
            {
                // Debug.Log(">> Keep still, distance is too short: " + Vector2.Distance(_lastHitPoint, hit.point));
                return;
            }

            _lastHitPoint = hit.point;

            var nextPoint = hit.point;
            if (direction.x > 0)
            {
                nextPoint.x -= 1.0f;
            } else if (direction.y > 0)
            {
                nextPoint.y -= 1.0f;
            }

            nextPoint = PixelPerfectPosition(nextPoint);
            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                nextPoint.y = origin.y;
            } else
            {
                nextPoint.x = origin.x;
            }

            _destination = nextPoint;
            _moving = true;
        }
        
        private void AnimateMovement()
        {
            if (!_moving)
            {
                return;
            }
            transform.position = Vector3.MoveTowards(transform.position, _destination, speed * Time.deltaTime);
            const float minimumDistance = 0.005f;
            if (Vector3.Distance(transform.position, _destination) < minimumDistance)
            {
                transform.position = _destination;
                _moving = false;
            }
        }
        
        private const int PixelsPerUnit = 100;
        private int RoundUp(float numToRoundF, int multiple)
        {
            int numToRound = Mathf.FloorToInt(numToRoundF);
            var isPositive = (numToRound >= 0) ? 1 : 0;
            return ((numToRound + isPositive * (multiple - 1)) / multiple) * multiple;
        }
        
        private Vector3 PixelPerfectPosition(Vector3 point)
        {
            const int gridSize = 125;
            var position = point * PixelsPerUnit;
            position.x = RoundUp((int)position.x, gridSize) / (float)PixelsPerUnit;
            position.y = RoundUp((int)position.y, gridSize) / (float)PixelsPerUnit;
            position.z = RoundUp((int)position.z, gridSize) / (float)PixelsPerUnit;
            return position;
        }
    }
}
