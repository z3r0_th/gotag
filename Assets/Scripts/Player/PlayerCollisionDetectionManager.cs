﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerCollisionDetectionManager : MonoBehaviour
    {
        [SerializeField] private float playerRadius = 1;
        private Action<Player, Player> _collisionEvent;
        private Player[] _players;

        public void SetPlayers(Player[] players)
        {
            _players = players;
        }
        
        public event Action<Player, Player> CollisionEvent
        {
            add => _collisionEvent += value;
            remove => _collisionEvent -= value;
        }
        
        private void Update()
        {
            if (_players == null)
            {
                return;
            }

            for (var i = 0; i < _players.Length - 1 ; ++i)
            {
                for (var j = i + 1; j < _players.Length; ++j)
                {
                    var p1 = _players[i];
                    var p2 = _players[j];
                    if (p1 == null) break;
                    if (p2 == null) continue;

                    if (Vector2.Distance(p1.transform.position, p2.transform.position) < playerRadius)
                    {
                        Collide(p1, p2);
                    }                     
                }
                
            }
        }

        private void Collide(Player p1, Player p2)
        {
            _collisionEvent.Invoke(p1, p2);
        }
    }
}