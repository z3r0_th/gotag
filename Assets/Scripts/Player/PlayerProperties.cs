using UnityEngine.InputSystem;
using UnityEngine;
using System;

namespace Player
{
    
    [CreateAssetMenu(fileName = "Player", menuName = "Game/Player", order = 0)]
    public class PlayerProperties : ScriptableObject
    {
        public string PlayerName
        {
            get => playerName;
            set => playerName = value;
        }

        public CharacterProperties Character
        {
            get => character;
            set => character = value;
        }

        public InputActionReference PlayerMovementInputReference => playerMovementInputMap;
        
        [SerializeField] private InputActionReference playerMovementInputMap;
        [SerializeField] private string playerName;
        
        [SerializeField] private CharacterProperties character;
    }
}
