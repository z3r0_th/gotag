using UnityEngine.InputSystem;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement))]
    public class PlayerControl : MonoBehaviour
    {
        public InputActionReference MovementInputReference
        {
            get => movement;
            set => movement = value;
        }
        [SerializeField] private InputActionReference movement;
        private PlayerMovement _playerMovement;

        private void Awake()
        {
            _playerMovement = GetComponent<PlayerMovement>();
        }
        
        void Update()
        {
            var movementDirection = movement.action.ReadValue<Vector2>();
            if (movementDirection.magnitude <= float.Epsilon)
            {
                return;
            }
            
            if (Mathf.Abs(movementDirection.x) > Mathf.Abs(movementDirection.y))
            {
                _playerMovement.Move(movementDirection.x > 0 ? MoveDirection.Right : MoveDirection.Left);
            } else
            {
                _playerMovement.Move(movementDirection.y > 0 ? MoveDirection.Up : MoveDirection.Down);
            }
        }
    }
}
