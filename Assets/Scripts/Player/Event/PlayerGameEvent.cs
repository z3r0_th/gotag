﻿using UnityEngine;
using System;

namespace Player.Event
{
    [CreateAssetMenu(fileName = "Game Event", menuName = "Game/GameEvent/Player Event", order = 0)]
    public class PlayerGameEvent : GameEventTemplate<Player>
    {
        
    }
}