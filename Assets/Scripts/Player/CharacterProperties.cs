﻿using UnityEngine;

namespace Player
{
    [CreateAssetMenu(fileName = "Character", menuName = "Game/Character", order = 0)]
    public class CharacterProperties : ScriptableObject
    {
        [SerializeField] private string characterName;
        [SerializeField] private Sprite characterSprite;
        [SerializeField] private Sprite characterHeadSprite;
        [SerializeField] private Sprite characterHeadSmileSprite;

        public Sprite CharacterSprite => characterSprite;

        public Sprite CharacterHeadSprite => characterHeadSprite;
        
        public Sprite CharacterHeadSmileSprite => characterHeadSmileSprite;

        public string CharacterName => characterName;
    }
}